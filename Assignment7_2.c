//Program to find the frequency of a given character in a string.

#include <stdio.h>
int main() {
    char str[1000], character;
    int count = 0;

    printf("Enter a string to find frequency of a given character: \n");
    fgets(str, sizeof(str), stdin);

    printf("Enter a character to find its frequency: \n");
    scanf("%c", &character);

    for (int i = 0; str[i] != '\0'; ++i) {
        if (character == str[i])
            ++count;
    }

    printf("Frequency of %c : \n%d \n", character, count);
    return 0;
}
