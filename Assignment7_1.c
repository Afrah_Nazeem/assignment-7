//Program to print the string in reverse order

#include <stdio.h>
#include <string.h>
#define MAX_SIZE 100

int main()
{
    char str[100], reverse[100];
    int length, i, index, word_Start, word_End;

    printf(" Enter the string to be reversed: \n ");
    gets(str);

    length = strlen(str);
    index = 0;

    word_Start = length - 1;
    word_End   = length - 1;

    while(word_Start > 0)
    {
        if(str[word_Start] == ' ')
        {
            i = word_Start + 1;
            while(i <= word_End)
            {
                reverse[index] = str[i];
                i++;
                index++;
            }
            reverse[index++] = ' ';

            word_End = word_Start - 1;
        }

        word_Start--;
    }

    for(i=0; i<=word_End; i++)
    {
        reverse[index] = str[i];
        index++;
    }

    reverse[index] = '\0';
    printf("\n The string in Reverse order \n %s \n", reverse);

    return 0;
}
